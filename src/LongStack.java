import java.util.*;

public class LongStack {
	
	//Sources:
	//http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
	//https://en.wikipedia.org/wiki/Reverse_Polish_notation

   private LinkedList<Long> mag;

   public static void main(String[] argum) 
   {
	   	System.out.println(interpret("5 1 - 7 * 6 3 / +"));
		// System.out.println(interpret("5 1 -"));
		// System.out.println(interpret("6 s /"));
		// System.out.println(interpret("4 7 9 9 *"));
		// System.out.println(interpret("]"));

		// System.out.println(interpret(" "));
		// System.out.println(interpret(""));
		// System.out.println(interpret("2 *"));
		// System.out.println(interpret("1 2"));
		// System.out.println(interpret("1 2 2 3 * 3 /"));	

   }//main

 //Constructor for a new stack
   LongStack() 
   {
      mag = new LinkedList<Long>();
   }//LongStack

   //Copy/Clone of the stack
   @Override
   public Object clone() throws CloneNotSupportedException 
   {
      LongStack temporary = new LongStack();
      temporary.mag = (LinkedList<Long>) mag.clone();
      return temporary;
   }//clone

   //Check whether the stack is empty or not
   public boolean stEmpty() 
   {
      if (mag.size() == 0) {
         return true;
      }
      return false;
   }//stEmtpy

 //Adding an element to the stack
   public void push(long a) 
   {
      mag.push(a);
   }//push

   //Removing an element from the stack
   public long pop() 
   {
	  if(stEmpty())
	  {
		  throw new RuntimeException("Magasin is empty!");
	  }
	  else
	  {
		  return mag.pop();
	  }
    }//pop

 //Arithmetic operation s ( + - * / ) between two topmost elements of the stack (result is left on top)
 public void op(String s) {
     if (mag.size() < 2) {
         throw new RuntimeException("There is not enough numbers to perform operation " + s );
     } else {
         try {
             long number2 = pop();
             long number1 = pop();
             if (s.equals("+")) push(number1 + number2);
             if (s.equals("-")) push(number1 - number2);
             if (s.equals("*")) push(number1 * number2);
             if (s.equals("/")) push(number1 / number2);
         } catch (Exception e) {
             throw new InputMismatchException("Vale operaator " + s);
         }
     }//else
 }//op


    //Reading the top without removing it
   public long tos() {
      if (mag.size() != 0) 
      {
         return mag.getFirst();
      } else {
         throw new RuntimeException("Magasin is empty!");
      }
   }//tos

   //Check whether two stacks are equal
   @Override
   public boolean equals(Object o) {
      if (((LongStack) o).mag.size() != mag.size())
         return false;
      for (int i = 0; i < mag.size(); i++) {
         if (((LongStack) o).mag.get(i) != mag.get(i))
            return false;
      }
      return true;
   }//equals

   //Conversion of the stack to string (top last)
   @Override
   public String toString() 
   {
      if (stEmpty()) {
         return "Magasin is empty!";
      }
      StringBuffer buffer = new StringBuffer();
      for (int index = mag.size() - 1; index >= 0; index--) {
         buffer.append(String.valueOf(mag.get(index)));
      }
      return buffer.toString();
   }//toString

   //Calculate the value of an arithmetic expression pol in RPN (Reverse Polish Notation)
   public static long interpret(String pol) {

      //Check if parameter is String, throw exception if not
      if (pol.isEmpty() || pol == null) {
         throw new RuntimeException("Empty expression!");
      }
      
      //Creating new LongStack Object
      //Remove String whiteSpace
      //Create String operators("+-*/")
      //Creating array of strings, where we "cut" the string into smaller pieces.
      	 LongStack stack = new LongStack();
         pol = pol.trim();
         String operators = "+-*/";
         String[] elements = pol.split("\\s+");

       //Loop through array of elements
       for (int i = 0; i < elements.length; i++) {
           //Check if string has number or operator.
           //If number, push to stack, if operator, make operation.
           if (!operators.contains(elements[i])) {
               try {
                   stack.push(Long.parseLong((elements[i])));
               } catch (NumberFormatException e) {
                   throw new RuntimeException("Broken expression " + pol);
               }
           } else {

               try {
                   long first = stack.pop();
                   long second = stack.pop();
                   switch (elements[i]) {
                       case "+":
                           stack.push(first + second);
                           break;
                       case "-":
                           stack.push(second - first);
                           break;
                       case "*":
                           stack.push(first * second);
                           break;
                       case "/":
                           stack.push(second / first);
                           break;
                   }
               } catch (Exception e) {
                   throw new RuntimeException("Expression " + pol + " is not valid!");
               }
           }
       }
       if (stack.mag.size() > 1)
           throw new RuntimeException("Too many numbers in expression " + pol);
       return stack.pop();
   }
   }//class LongStack
